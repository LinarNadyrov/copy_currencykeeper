package ru.vipconnect.currencykeeper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/** Курс обмена валютной пары */
@Entity
@Table(name = "tbl_currency_rate")
public class CurrencyRate implements Serializable
{
	private static final long serialVersionUID = -2698114334196655123L;

	private Long id;
	private Currency currencyFrom;
	private Currency currencyTo;
	private Integer nominal;
	private BigDecimal value;
	private Date date;
	private String providerCode;
	private Date created;
	
	/** Id курса */
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="currency_rate_seq")
	@SequenceGenerator(name="currency_rate_seq", sequenceName="currency_rate_seq")	
	@Column(name="currency_rate_id")	
	public Long getId()
	{
		return id;
	}
	public void setId(Long id)
	{
		this.id = id;
	}
	
	/** Валюта из */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "currency_id_from")
	@NotNull
	public Currency getCurrencyFrom()
	{
		return currencyFrom;
	}
	public void setCurrencyFrom(Currency currencyFrom)
	{
		this.currencyFrom = currencyFrom;
	}

	/** Валюта в */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "currency_id_to")
	@NotNull
	public Currency getCurrencyTo()
	{
		return currencyTo;
	}
	public void setCurrencyTo(Currency currencyTo)
	{
		this.currencyTo = currencyTo;
	}
	
	/** Номинал курса (кол-во единиц исходной валюты) */
	@NotNull
	@Column(name = "currency_rate_nominal")	
	public Integer getNominal()
	{
		return nominal;
	}
	public void setNominal(Integer nominal)
	{
		this.nominal = nominal;
	}

	/** Курс пересчёта */
	@NotNull
	@Column(name = "currency_rate_value")	
	public BigDecimal getValue()
	{
		return value;
	}
	public void setValue(BigDecimal value)
	{
		this.value = value;
	}

	/** Дата курса */
	@NotNull
	@Column(name = "currency_rate_date")	
	public Date getDate()
	{
		return date;
	}
	public void setDate(Date date)
	{
		this.date = date;
	}
	
	/** Код поставщика курса */
	@NotNull
	@Column(name = "currency_rate_provider_code")	
	public String getProviderCode()
	{
		return providerCode;
	}
	public void setProviderCode(String providerCode)
	{
		this.providerCode = providerCode;
	}
	
	/** Дата создания записи */
	@Column(name="created")	
	public Date getCreated()
	{
		return created;
	}
	public void setCreated(Date created)
	{
		this.created = created;
	}
	
	@Override
	public String toString()
	{
		return "CurrencyRate(" + id + ", "
								+ currencyFrom.getId() + ", "
								+ currencyTo.getId() + ", " 
								+ nominal + ", "
								+ value.toPlainString() + ", " 
								+ date + ", "
								+ providerCode + 
								")";
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
			return true;
		if (o == null || !getClass().isInstance(o))
			return false;

		final Currency other = (Currency) o;

		if ((getId() == null && other.getId() != null) || !getId().equals(other.getId()))
			return false;
		return true;
	}

	@Override
	public int hashCode()
	{
		return getId().hashCode();
	}	
}
