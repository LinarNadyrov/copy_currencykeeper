package ru.vipconnect.currencykeeper.ejb;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ru.vipconnect.currencykeeper.entity.Currency;

@Stateless
@LocalBean
public class CurrencyBLBean implements ICurrency
{
	@PersistenceContext(unitName = "currencyKeeper")
	protected EntityManager em;

	public EntityManager getEm()
	{
		return em;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Currency findByCode(String code)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Currency> cq = cb.createQuery(Currency.class);
		Root<Currency> r = cq.from(Currency.class);
		
		cq.select(r);
		cq.where(cb.equal(r.get("code"), code));
		
		try
		{
			return em.createQuery(cq).getSingleResult();
		}
		catch (NoResultException e)
		{
			return null;
		}
	}
	
	public List<Currency> listAll()
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Currency> cq = cb.createQuery(Currency.class);
		Root<Currency> c = cq.from(Currency.class);
		cq.select(c);
		cq.orderBy(cb.asc(c.get("code")));
		
		return em.createQuery(cq).getResultList();		
	}
}
