
package ru.vipconnect.currencykeeper.guvnor;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.drools.definition.rule.Rule;
import org.drools.event.rule.AfterActivationFiredEvent;
import org.drools.event.rule.BeforeActivationFiredEvent;
import org.drools.event.rule.DefaultAgendaEventListener;


public class TrackingAgendaEventListener extends DefaultAgendaEventListener {
	
	private static Logger log = LogManager.getLogger(TrackingAgendaEventListener.class);
	private String ruleName = "";
	private String packageName = "";
	public String getFiredRuleName() {
		return ruleName;
	}
	public String getFiredPackageName() {
		return packageName;
	}
	@Override
	public void afterActivationFired(AfterActivationFiredEvent event) {
		Rule rule = event.getActivation().getRule();
		ruleName = rule.getName();
		packageName = rule.getPackageName();
		String ruleStr = String.format("%s:%s", rule.getPackageName(), rule.getName());
		log.info("Сработало правило: " + ruleStr + "(" + event.getActivation().getObjects() + ")");
	}
	@Override
	public void beforeActivationFired(BeforeActivationFiredEvent event) {
		Rule rule = event.getActivation().getRule();
		String ruleStr = String.format("%s:%s", rule.getPackageName(), rule.getName());
		log.info("Проверка правила: " + ruleStr + "(" + event.getActivation().getObjects() + ")");
	}

}
