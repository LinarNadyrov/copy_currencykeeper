package ru.vipconnect.currencykeeper.provider.VIP;

import java.io.Serializable;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/** Курсы из SugarCRM */
@XmlRootElement(name = "Rows")
public class IntExRates implements Serializable
{
	private static final long serialVersionUID = -3294100082678113845L;
	
	private List<IntExRate> rowList;
	
	public static class IntExRate implements Serializable
	{
		private static final long serialVersionUID = 8060815176237255596L;
		
		private Date rateDate;
		private BigDecimal rateValue;
		private String currencyCodeFrom;
		private String currencyCodeTo;
		private Integer nominal;
		
		@XmlElement(name = "internal_rate_date_c")
		@XmlJavaTypeAdapter(DateAdapter.class)
		public Date getRateDate()
		{
			return rateDate;
		}
		public void setRateDate(Date rateDate)
		{
			this.rateDate = rateDate;
		}
		
		@XmlElement(name = "internal_rate_c")
		public BigDecimal getRateValue()
		{
			return rateValue;
		}
		public void setRateValue(BigDecimal rateValue)
		{
			this.rateValue = rateValue;
		}
		
		@XmlElement(name = "rating_value_currency_c")
		public String getCurrencyCodeFrom()
		{
			return currencyCodeFrom;
		}
		public void setCurrencyCodeFrom(String currencyCodeFrom)
		{
			this.currencyCodeFrom = currencyCodeFrom;
		}
		
		@XmlElement(name = "currency_c")
		public String getCurrencyCodeTo()
		{
			return currencyCodeTo;
		}
		public void setCurrencyCodeTo(String currencyCodeTo)
		{
			this.currencyCodeTo = currencyCodeTo;
		}
		
		@XmlElement(name = "rating_value_c")
		public Integer getNominal()
		{
			return nominal;
		}
		public void setNominal(Integer nominal)
		{
			this.nominal = nominal;
		}
	}

	@XmlElement(name = "Row")
	public List<IntExRate> getRowList()
	{
		return rowList;
	}

	public void setRowList(List<IntExRate> rowList)
	{
		this.rowList = rowList;
	}
	
	public static IntExRates fromXml(String xml) throws JAXBException, ClassCastException
	{
		JAXBContext jaxbContext = JAXBContext.newInstance(IntExRates.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		StringReader reader = new StringReader(xml);
		
		return (IntExRates) jaxbUnmarshaller.unmarshal(reader);
	}	
}
