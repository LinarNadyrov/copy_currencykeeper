package ru.vipconnect.currencykeeper.guvnor;

import java.io.IOException;

/**
 *
 * @author kit
 */
public class RuleNotFoundException extends IOException {

	private static final long serialVersionUID = 7865548497465918079L;

	/**
	 * Creates a new instance of
	 * <code>RuleNotFoundException</code> without detail message.
	 */
	public RuleNotFoundException() {
	}

	/**
	 * Constructs an instance of
	 * <code>RuleNotFoundException</code> with the specified detail message.
	 *
	 * @param msg the detail message.
	 */
	public RuleNotFoundException(String msg) {
		super(msg);
	}
}
