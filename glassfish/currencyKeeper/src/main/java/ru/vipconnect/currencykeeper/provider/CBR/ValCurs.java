package ru.vipconnect.currencykeeper.provider.CBR;

import java.io.Serializable;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
* Курсы валют на дату
*/
@XmlRootElement(name = "ValCurs")
public class ValCurs implements Serializable
{
	private static final long serialVersionUID = -844304579022554460L;
	
	private Date date;
	private String name;
	private List<Valute> valuteList;
	
    /** Дата курсов */
	@XmlAttribute(name="Date")
	@XmlJavaTypeAdapter(DateAdapter.class)
	public Date getDate()
	{
		return date;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

    /** Наименование рынка */
	@XmlAttribute(name="name")
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	/** Список валют и их курсов к рублю */
	@XmlElement(name = "Valute")
	public List<Valute> getValuteList()
	{
		return valuteList;
	}

	public void setValuteList(List<Valute> valuteList)
	{
		this.valuteList = valuteList;
	}

	/** Курс валюты */
	public static class Valute implements Serializable
	{
		private static final long serialVersionUID = -6340192574440346219L;
		
		private String id;
		private String numCode;
		private String charCode;
		private Integer nominal;
		private String name;
		private BigDecimal value;
		
	    /** Id валюты */
		@XmlAttribute(name="ID")		
		public String getId()
		{
			return id;
		}
		public void setId(String id)
		{
			this.id = id;
		}

		/** Цифровой код валюты */
		@XmlElement(name = "NumCode")
		public String getNumCode()
		{
			return numCode;
		}
		public void setNumCode(String numCode)
		{
			this.numCode = numCode;
		}

		/** Буквенный код валюты */
		@XmlElement(name = "CharCode")
		public String getCharCode()
		{
			return charCode;
		}
		public void setCharCode(String charCode)
		{
			this.charCode = charCode;
		}

		/** Номинал обмена */
		@XmlElement(name = "Nominal")
		public Integer getNominal()
		{
			return nominal;
		}
		public void setNominal(Integer nominal)
		{
			this.nominal = nominal;
		}

		/** Наименование валюты */
		@XmlElement(name = "Name")
		public String getName()
		{
			return name;
		}
		public void setName(String name)
		{
			this.name = name;
		}

		/** Курс */
		@XmlElement(name = "Value")
		@XmlJavaTypeAdapter(BigDecimalAdapter.class)		
		public BigDecimal getValue()
		{
			return value;
		}
		public void setValue(BigDecimal value)
		{
			this.value = value;
		}
	}
	
	public static ValCurs fromXml(String xml) throws JAXBException, ClassCastException
	{
		JAXBContext jaxbContext = JAXBContext.newInstance(ValCurs.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		StringReader reader = new StringReader(xml);
		
		return (ValCurs) jaxbUnmarshaller.unmarshal(reader);
	}	
}
