package ru.vipconnect.currencykeeper.guvnor;

/**
 *
 * @author kit
 */
public class ApplyChangeSetException extends Exception {

	private static final long serialVersionUID = -3465729306814315153L;

	/**
	 * Creates a new instance of
	 * <code>RuleNotFoundException</code> without detail message.
	 */
	public ApplyChangeSetException() {
	}

	/**
	 * Constructs an instance of
	 * <code>RuleNotFoundException</code> with the specified detail message.
	 *
	 * @param msg the detail message.
	 */
	public ApplyChangeSetException(String msg) {
		super(msg);
	}
}
