package ru.vipconnect.currencykeeper.ejb;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import ru.vipconnect.currencykeeper.entity.Currency;
import ru.vipconnect.currencykeeper.entity.CurrencyRate;

@Stateless
@LocalBean
public class CurrencyRateBLBean implements ICurrencyRate
{
	@PersistenceContext(unitName = "currencyKeeper")
	protected EntityManager em;

	public EntityManager getEm()
	{
		return em;
	}
	
	public CurrencyRate insert(CurrencyRate src)
	{
		src.setCreated(new Date());
		em.persist(src);
		return src;
	}
	
	public CurrencyRate findByAK(String[] currencyPair, Date date, String providerCode)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<CurrencyRate> cq = cb.createQuery(CurrencyRate.class);
		Root<CurrencyRate> r = cq.from(CurrencyRate.class);
		Join<Currency,CurrencyRate> cfrom = r.join("currencyFrom");
		Join<Currency,CurrencyRate> cto = r.join("currencyTo");
		
		cq.select(r);
		cq.where(
				cb.equal(r.get("providerCode"), providerCode),
				cb.equal(r.get("date"), date),
				cb.equal(cfrom.get("code"), currencyPair[0]),
				cb.equal(cto.get("code"), currencyPair[1]));
		
		try
		{
			return em.createQuery(cq).getSingleResult();
		}
		catch (NoResultException e)
		{
			return null;
		}
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CurrencyRate findRateOnDate(String currencyCodeFrom, String currencyCodeTo, Date date, String providerCode)
	{
		if (currencyCodeFrom != null && currencyCodeFrom.equals(currencyCodeTo))
		{
			Currency c = new Currency();
			c.setCode(currencyCodeFrom);
			CurrencyRate cr = new CurrencyRate();
			cr.setCurrencyFrom(c);
			cr.setCurrencyTo(c);
			cr.setDate(date);
			cr.setProviderCode(providerCode);
			cr.setNominal(1);
			cr.setValue(BigDecimal.ONE);
			
			return cr;
		}
		else
		{
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<CurrencyRate> cq = cb.createQuery(CurrencyRate.class);
			Root<CurrencyRate> r = cq.from(CurrencyRate.class);
			Join<Currency,CurrencyRate> cfrom = r.join("currencyFrom");
			Join<Currency,CurrencyRate> cto = r.join("currencyTo");
			
			cq.select(r);
			cq.where(
					cb.equal(r.get("providerCode"), providerCode),
					cb.lessThanOrEqualTo(r.<Date>get("date"), date),
					cb.equal(cfrom.get("code"), currencyCodeFrom),
					cb.equal(cto.get("code"), currencyCodeTo));
			cq.orderBy(cb.desc(r.<Date>get("date")));
			
			try
			{
				return em.createQuery(cq).setFirstResult(0).setMaxResults(1).getSingleResult();
			}
			catch (NoResultException e)
			{
				return null;
			}
		}
	}
	
	public List<CurrencyRate> listOnRange(String providerCode, Date dateFrom, Date dateTo, String currencyCodeFrom, String currencyCodeTo)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<CurrencyRate> cq = cb.createQuery(CurrencyRate.class);
		Root<CurrencyRate> r = cq.from(CurrencyRate.class);
		Join<Currency,CurrencyRate> cfrom = r.join("currencyFrom");
		Join<Currency,CurrencyRate> cto = r.join("currencyTo");

		cq.select(r);

		cq.where(cb.isNotNull(r.get("id")));

		if (providerCode != null)
			cq.where(cq.getRestriction(), cb.equal(r.get("providerCode"), providerCode));

		if (dateFrom != null)
			cq.where(cq.getRestriction(), cb.greaterThanOrEqualTo(r.<Date>get("date"), dateFrom));

		if (dateTo != null)
			cq.where(cq.getRestriction(), cb.lessThanOrEqualTo(r.<Date>get("date"), dateTo));
		
		if (currencyCodeFrom != null)
			cq.where(cq.getRestriction(), cb.equal(cfrom.get("code"), currencyCodeFrom));

		if (currencyCodeTo != null)
			cq.where(cq.getRestriction(), cb.equal(cto.get("code"), currencyCodeTo));
		
		cq.orderBy(cb.asc(r.get("date")),
					cb.asc(r.get("providerCode")),
					cb.asc(cfrom.get("code")),
					cb.asc(cto.get("code")));
		
		return em.createQuery(cq).getResultList();		
	}	
}
