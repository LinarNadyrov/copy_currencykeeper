package ru.vipconnect.currencykeeper.model;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.vipconnect.currencykeeper.exception.CurrencyKeeperException;

public class CurrencyPairSrcParams
{
	private static final Pattern p = Pattern.compile("(\\w{3})[,/-](\\w{3})");
	
	private String provider;
	private List<String[]> currencyPairList = new ArrayList<String[]>();
	
	/** Поставщик курсов (код) */
	public String getProvider()
	{
		return provider;
	}
	public void setProvider(String provider)
	{
		this.provider = provider;
	}
	
	/** Список валютных пар */
	public List<String[]> getCurrencyPairList()
	{
		return currencyPairList;
	}

	/**
	 * Добавить в список валютную пару
	 * @param s Строка в формате BYN/RUB
	 * @throws CurrencyKeeperException
	 */
	public void addCurrencyPair(String s) throws CurrencyKeeperException
	{
		Matcher m = p.matcher(s);
		
		if (m.matches())
		{
			String[] sa = new String[2];
			sa[0] = m.group(1);
			sa[1] = m.group(2);
			currencyPairList.add(sa);
		}
		else
		{
			throw new CurrencyKeeperException("Unable to parse currency pair string: " + s);
		}
	}
	
    @Override
    public String toString()
    {
    	return "CurrencyPairSrcParams{" + 
    			"provider=" + provider + 
    			", currencyPairList=" + currencyPairList + '}';
    }
}
