package ru.vipconnect.currencykeeper.ejb;

import java.util.List;

import javax.ejb.Remote;

import ru.vipconnect.currencykeeper.entity.Currency;

@Remote
public interface ICurrency
{
	public Currency findByCode(String code);
	public List<Currency> listAll();
}
