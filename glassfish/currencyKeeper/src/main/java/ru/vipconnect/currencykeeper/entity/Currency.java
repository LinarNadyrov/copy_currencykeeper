package ru.vipconnect.currencykeeper.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/** Валюта */
@Entity
@Table(name = "tbl_currency")
public class Currency implements Serializable
{
	private static final long serialVersionUID = -5869478570981773864L;
	
	private Integer id;
	private String code;
	private Integer codeNum;
	private String name;
	private Date created;
	
	/** Id валюты */
	@Id
	@Column(name="currency_id")	
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	
	/** Код валюты */
	@NotNull
	@Column(name = "currency_code")	
	public String getCode()
	{
		return code;
	}
	public void setCode(String code)
	{
		this.code = code;
	}

	/** Цифровой код валюты */
	@NotNull
	@Column(name = "currency_num_code")	
	public Integer getCodeNum()
	{
		return codeNum;
	}

	public void setCodeNum(Integer codeNum)
	{
		this.codeNum = codeNum;
	}

	/** Наименование валюты */
	@NotNull
	@Column(name = "currency_name")	
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	
	/** Дата создания записи */
	@Column(name="created")	
	public Date getCreated()
	{
		return created;
	}
	public void setCreated(Date created)
	{
		this.created = created;
	}

	@Override
	public String toString()
	{
		return "Currency(" + id + ", " + code+ ")";
	}

	@Override	
	public boolean equals(Object o)
	{
		if (this == o)
			return true;
		if (o == null || !getClass().isInstance(o))
			return false;

		final Currency other = (Currency) o;

		if ((getId() == null && other.getId() != null) || !getId().equals(other.getId()))
			return false;
		return true;
	}

	@Override	
	public int hashCode()
	{
		return getId().hashCode();
	}	
}
