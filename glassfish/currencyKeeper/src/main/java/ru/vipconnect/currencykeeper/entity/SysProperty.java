package ru.vipconnect.currencykeeper.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/** Конфигурация */
@Entity
@Table(name = "tbl_sys_property")
public class SysProperty implements Serializable
{
	private static final long serialVersionUID = -7768395288087346552L;

	private SysPropertyPK pk;
	private String propertyValue;
		
	@EmbeddedId
	public SysPropertyPK getPk()
	{
		return pk;
	}
	public void setPk(SysPropertyPK pk)
	{
		this.pk = pk;
	}
	
	/** Значение параметра */
	@Column(name="property_value")
	@NotNull
	public String getPropertyValue()
	{
		return propertyValue;
	}
	public void setPropertyValue(String propertyValue)
	{
		this.propertyValue = propertyValue;
	}
	
	@Override
	public String toString()
	{
		return "SysProperty(" + getPk().getConfigName() + ", " + getPk().getPropertyName() + ")";
	}

	@Override	
	public boolean equals(Object o)
	{
		if (this == o)
			return true;
		if (o == null || !getClass().isInstance(o))
			return false;

		final SysProperty other = (SysProperty) o;

		if (!getPk().getConfigName().equals(other.getPk().getConfigName()) ||
				!getPk().getPropertyName().equals(other.getPk().getPropertyName()) ||
				!getPropertyValue().equals(other.getPropertyValue()))
			return false;
		return true;
	}

	@Override	
	public int hashCode()
	{
		return (getPk().getConfigName()  + "." + getPk().getPropertyName()).hashCode();
	}	
}
