package ru.vipconnect.currencykeeper.provider.CBR;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.xml.bind.JAXBException;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ru.vipconnect.currencykeeper.entity.CurrencyRate;
import ru.vipconnect.currencykeeper.provider.CurrencyRateProvider;
import ru.vipconnect.currencykeeper.provider.CBR.ValCurs.Valute;

@Singleton
@Startup
@LocalBean
public class CurrencyRateProviderCBR extends CurrencyRateProvider
{
	private static final Logger log = LogManager.getLogger(CurrencyRateProviderCBR.class);
	
	@Override
	public Integer getNewRates(List<String[]> currencyPairList)
	{
		log.info("Getting CBR rates...");
		int num = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		Date currentDate = DateUtils.truncate(new Date(), Calendar.DATE);
		for (int i=31; i>=-1; i--)
		{
			Date rateDate = DateUtils.addDays(currentDate, -i);			
			boolean isRequest = false;
			
			for (String[] currencyPair : currencyPairList)
			{
				if ("RUB".equals(currencyPair[1]))
				{
					CurrencyRate cr = crBean.findByAK(currencyPair, rateDate, getProviderCode());
					if (cr == null) isRequest = true;
				}
			}
			
			if (isRequest)
			{
				log.info("Requesting rates for " + sdf.format(rateDate));

				CloseableHttpClient httpclient = HttpClients.createDefault();
				HttpGet httpGet = new HttpGet(
						config.getProperty("provider.cbr.url", "http://www.cbr.ru/scripts/XML_daily.asp")
						+ "?date_req=" + sdf.format(rateDate));
				try
				{
					CloseableHttpResponse resp = httpclient.execute(httpGet);
					ValCurs vc = ValCurs.fromXml(readResult(resp));
					
					for (Valute v : vc.getValuteList())
					{
						for (String[] currencyPair : currencyPairList)
						{
							if (v.getCharCode().equals(currencyPair[0]) && "RUB".equals(currencyPair[1]))
							{
								CurrencyRate cr = crBean.findByAK(currencyPair, vc.getDate(), getProviderCode());
								if (cr == null)
								{
									cr = new CurrencyRate();
									cr.setCurrencyFrom(cBean.findByCode(currencyPair[0]));
									cr.setCurrencyTo(cBean.findByCode(currencyPair[1]));
									cr.setNominal(v.getNominal());
									cr.setValue(v.getValue());
									cr.setDate(vc.getDate());
									cr.setProviderCode(getProviderCode());
									log.info("Inserting rate for " + currencyPair[0] + ", date: " + vc.getDate() + ", value:" + v.getValue());
									crBean.insert(cr);
									num++;
								}
							}
						}
					}
				}
				catch (IOException | ClassCastException | JAXBException e)
				{
					log.error("Exception: ", e);
				}				
			}
		}
		
		log.info("Getting CBR rates...done!");
		log.info("Got " + num + " new rates");
		
		return num;
	}

	private static String readResult(HttpResponse response) throws IOException
	{
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null)
		{
			result.append(line);
		}

		log.info("Got response:\n{}", result.toString());

		EntityUtils.consumeQuietly(response.getEntity());

		return result.toString();
	}
	
	@Override
	public String getProviderCode()
	{
		return "CBR";
	}

	@Override
	protected Logger getLogger()
	{
		return log;
	}

}
