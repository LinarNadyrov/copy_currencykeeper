package ru.vipconnect.currencykeeper.guvnor;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.drools.KnowledgeBase;
import org.drools.agent.KnowledgeAgent;
import org.drools.agent.KnowledgeAgentConfiguration;
import org.drools.agent.KnowledgeAgentFactory;
import org.drools.definition.KnowledgePackage;
import org.drools.definition.rule.Rule;
import org.drools.io.Resource;
import org.drools.io.ResourceChangeScannerConfiguration;
import org.drools.io.ResourceFactory;

import ru.vipconnect.currencykeeper.ejb.ConfigBean;

/**
 * Класс для доступа к пакетам правил.
 * Доступ к пакетам правил осуществляется посредством сервиса Guvnor по имени пакета.
 * Загруженные пакеты кэшируются, и при последующих обращениях не тратится время на получение и разбор пакета.
 *
 */
@Singleton
@LocalBean
public class KnowledgePackagesSource {
	
	@EJB
	private KAgentSystemEventListener systemEventListener;
	@EJB
	private KAgentEventListener eventListener;
	@EJB
	private ConfigBean config;
	
	private static Logger log = LogManager.getLogger(KnowledgePackagesSource.class);
	private static final String SALT = "GUVNOR";

	static Map<String, KnowledgeAgent> kagentCache = new HashMap<String, KnowledgeAgent>();
	/**
	 * Очистка кэша использованных пакетов правил.
	 * Необходимо вызвать, если пакеты правил были изменены после запуска приложения.
	 */
	public void clearKAgentCache() {
		for (String key : kagentCache.keySet()) {
			log.debug("Удаляем кеш: " + key);
			kagentCache.get(key).dispose();
		}
		kagentCache = new HashMap<String, KnowledgeAgent>();
	}
	
	public static void removeFromKAgentCache(String packageName) {
		if (kagentCache.containsKey(packageName)) {
			kagentCache.remove(packageName);
		}
	}

	@PostConstruct
	private void postConstruct() {
		ResourceChangeScannerConfiguration sconf = ResourceFactory.getResourceChangeScannerService().newResourceChangeScannerConfiguration();
		
		sconf.setProperty( "drools.resource.scanner.interval", config.getProperty("guvnor.scanner.interval") );
		
		ResourceFactory.getResourceChangeScannerService().configure( sconf );

		ResourceFactory.getResourceChangeNotifierService().start();  
		ResourceFactory.getResourceChangeScannerService().start();

		clearKAgentCache();
		try {
			prepareChangesetTemplate();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
	}
 
	
	
	/**
	 * Кэширующий метод получения пакета правил по имени их конфигурационной таблицы.
	 * Реальное имя пакета хранится в ta.properties по ключу с префиксом "guvnor.package"
	 * @param cfgPackageName имя ключа в конфиге (без префикса)
	 */
	public KnowledgeBase getKnowledgeBaseByCfgName (String cfgPackageName) throws ApplyChangeSetException {
		String realPackageName = config.getProperty(cfgPackageName);
		return getKnowledgeBase(realPackageName);
	}

	/**
	 * Кэширующий метод получения пакета правил.
	 *
	 * @param packageName
	 * @return
	 */
	public KnowledgeBase getKnowledgeBase (String packageName) throws ApplyChangeSetException {
		log.debug("Получение пакета: " + packageName);
		log.debug("Содержимое кеша: " + kagentCache.keySet());
		if ( ! kagentCache.containsKey(packageName)) {
			kagentCache.put(packageName, readKnowledgeBase(packageName));
		}
		KnowledgeBase kbase = kagentCache.get(packageName).getKnowledgeBase();   // добавляем полученый пакет 

		return kbase;
	}

	/**
	 * Получает пакет правил из источника прописанного в "ChangeSet".
	 * Инициализация ChangeSet осуществляется в методе {@link KnowledgePackagesSource#prepareChangesetTemplate() }
	 *
	 * @param packageName
	 * @return
	 * @throws IOException
	 */
	protected KnowledgeAgent readKnowledgeBase (String packageName) throws ApplyChangeSetException {
		KnowledgeAgentConfiguration agentConfig = KnowledgeAgentFactory.newKnowledgeAgentConfiguration();
			agentConfig.setProperty("drools.agent.newInstance", "false");

		KnowledgeAgent kagent = KnowledgeAgentFactory.newKnowledgeAgent(packageName);
		kagent.addEventListener(eventListener);
		kagent.setSystemEventListener(systemEventListener);
		String changeSet = changeSetTemplate.replaceAll("\\{packageName\\}", packageName);
		log.info(" ==== changeSet: "+changeSet);

		Resource resource = ResourceFactory.newByteArrayResource(changeSet.getBytes());
		try {
			kagent.applyChangeSet(resource);
		} catch (RuntimeException ex) {
			log.fatal(ex);
			throw new ApplyChangeSetException("Произошла ошибка при получении пакета: " + packageName);
		}

//		printBaseInfo(kagent.getKnowledgeBase());

		return kagent;
	}
	

	/////////////////////////////////////////////////////////////////////////////////////////////

	private String changeSetTemplate;
	/**
	 * Инициализирует ChangeSet для доступа к пакетам правил.
	 * Пакеты правил берутся из сервиса "Guvnor".
	 * Используются параметры из ta.properties  "guvnor.url", "guvnor.username", "guvnor.password".
	 *
	 * @throws IOException
	 */
	public void prepareChangesetTemplate() throws IOException {
		
		log.info("Initializing changeSetTemplate...");
		InputStream input = getClass().getResourceAsStream("/ChangeSetTemplate.xml");

		byte[] fileData = new byte[input.available()];

		input.read(fileData);
		input.close();

		
		
		
				
				changeSetTemplate = new String(fileData)
				.replaceAll("\\{guvnorUrl\\}", config.getProperty("guvnor.url"))
				.replaceAll("\\{username\\}",config.getProperty("guvnor.username"))
				.replaceAll("\\{password\\}",ConfigBean.decrypt(config.getProperty("guvnor.username") + SALT, config.getProperty("guvnor.password")));

		log.info("Initializing changeSetTemplate...done!");
		log.debug(changeSetTemplate);
		
	}

	@SuppressWarnings("unused")
	private static void printBaseInfo(KnowledgeBase kbase) {
		log.info("Packages: ");
		for (KnowledgePackage pack : kbase.getKnowledgePackages()) {
			log.info("\t"+pack.getName()+": ");
			for (Rule rule : pack.getRules()) {
				log.info("\t\t"+rule.getKnowledgeType()+": "+rule.getName());

//				Map ruleMetaData = rule.getMetaData();
//				log.info(ruleMetaData);
			}
		}
	}


	
}
