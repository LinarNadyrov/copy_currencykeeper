package ru.vipconnect.currencykeeper.provider.AMA;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.amadeus.xml.hsfreq_07_3_1a.CommandCryptic;
import com.amadeus.xml.hsfres_07_3_1a.CommandCrypticReply;

import ru.vipconnect.amadeus.cc.AmadeusConnection;
import ru.vipconnect.amadeus.Configuration;
import ru.vipconnect.currencykeeper.ejb.ConfigBean;
import ru.vipconnect.currencykeeper.entity.CurrencyRate;
import ru.vipconnect.currencykeeper.provider.CurrencyRateProvider;

@Singleton
@Startup
@LocalBean
public class CurrencyRateProviderAMA extends CurrencyRateProvider
{
	private static final Logger log = LogManager.getLogger(CurrencyRateProviderAMA.class);

	private void configureConnection()
	{
		String pwd = config.getProperty("provider." + getProviderCode().toLowerCase() + ".password");
		String office = config.getProperty("provider." + getProviderCode().toLowerCase() + ".office");
		pwd = ConfigBean.decrypt(office + getProviderCode(), pwd);
		
		Configuration.configure(1, // initial port count
				10000, // port timeout
				10000, // port inactivity timeout
				10000, // service connect timeout
				10000, // service request timeout
				true, // log soap
				config.getProperty("provider." + getProviderCode().toLowerCase() + ".soap_log_path"), // soap log dir
				config.getProperty("provider." + getProviderCode().toLowerCase() + ".url"), // service url
				config.getProperty("provider." + getProviderCode().toLowerCase() + ".originator"), // originator code
				office, // office code
				config.getProperty("provider." + getProviderCode().toLowerCase() + ".organization"), // organization id
				Base64.encodeBase64String(pwd.getBytes()), // password
				pwd.length(), // password length
				"RUB", // currency for price data
				250 // max number of returned recommendations
		);
	}

	@Override
	public Integer getNewRates(List<String[]> currencyPairList)
	{
		log.info("Getting AMADEUS rates...");
		int num = 0;

		configureConnection();

		try
		{

			AmadeusConnection conn = new AmadeusConnection();
			SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyy", Locale.ENGLISH);

			Date currentDate = DateUtils.truncate(new Date(), Calendar.DATE);
			for (int i = 31; i >= 0; i--)
			{
				Date rateDate = DateUtils.addDays(currentDate, -i);

				for (String[] currencyPair : currencyPairList)
				{
					log.info("Requesting rate for " + currencyPair[0] + "/" + currencyPair[1] + " on "
							+ sdf.format(rateDate).toUpperCase());
					CommandCryptic cc = new CommandCryptic();
					cc.setMessageAction(new CommandCryptic.MessageAction());
					cc.getMessageAction()
							.setMessageFunctionDetails(new CommandCryptic.MessageAction.MessageFunctionDetails());
					cc.getMessageAction().getMessageFunctionDetails().setMessageFunction("M");
					cc.setLongTextString(new CommandCryptic.LongTextString());
					cc.getLongTextString().setTextStringDetails("FQC1" + currencyPair[0] + "/" + currencyPair[1] + "/"
							+ sdf.format(rateDate).toUpperCase());
					log.debug("Request: " + cc.getLongTextString().getTextStringDetails());
					CommandCrypticReply ccr = conn.commandCryptic(cc);
					String reply = ccr.getLongTextString().getTextStringDetails();
					log.debug("Reply:\n" + reply);

					if (reply != null)
					{
						Pattern p = Pattern.compile("BSR USED 1 " + currencyPair[0] + " = (\\d+.\\d+) "
								+ currencyPair[1] + " EFF (\\d{2}\\w{3}\\d{2})");
						Matcher m = p.matcher(reply);
						if (m.find())
						{
							BigDecimal rateValue = new BigDecimal(m.group(1));
							Date effectiveRateDate = sdf.parse(m.group(2));

							CurrencyRate cr = crBean.findByAK(currencyPair, effectiveRateDate, getProviderCode());
							if (cr == null)
							{
								cr = new CurrencyRate();
								cr.setCurrencyFrom(cBean.findByCode(currencyPair[0]));
								cr.setCurrencyTo(cBean.findByCode(currencyPair[1]));
								cr.setNominal(1);
								cr.setValue(rateValue);
								cr.setDate(effectiveRateDate);
								cr.setProviderCode(getProviderCode());
								log.info("Inserting rate for " + currencyPair[0] + "/" + currencyPair[1] + ", date: "
										+ effectiveRateDate + ", value:" + rateValue);
								crBean.insert(cr);
								num++;
							}
						}
					}
				}
			}
		} catch (Exception e)
		{
			log.error("Exception: ", e);
		}

		log.info("Getting AMADEUS rates...done!");
		log.info("Got " + num + " new rates");

		return num;
	}

	@Override
	public String getProviderCode()
	{
		return "AMA";
	}

	@Override
	protected Logger getLogger()
	{
		return log;
	}

}
