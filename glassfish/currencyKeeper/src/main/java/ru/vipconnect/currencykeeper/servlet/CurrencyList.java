package ru.vipconnect.currencykeeper.servlet;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ru.vipconnect.currencykeeper.ejb.CurrencyBLBean;
import ru.vipconnect.currencykeeper.entity.Currency;

/**
 * Returns list of currencies
 */
@WebServlet("/CurrencyList")
public class CurrencyList extends HttpServlet
{
	private static final long serialVersionUID = 4749236281104112991L;

	@EJB
	private CurrencyBLBean curBL;
	
	public CurrencyList()
	{
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setCharacterEncoding("utf-8");
		
		String format = request.getParameter("format");
		StringBuffer sb = new StringBuffer();
		
		List<Currency> curList = curBL.listAll();

		if (format != null && "json".equals(format.toLowerCase()))
		{
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
			
			response.setContentType("application/json");
			
			sb.append("[\n");

			for (Currency cur : curList)
			{
				sb.append(gson.toJson(cur)).append(",\n");
			}
			
			sb.append("]\n");
		}
		else
		{
			response.setContentType("text/html");
			
			sb.append("<!DOCTYPE HTML>\n");
			sb.append("<html>\n");
			sb.append(" <head>\n");
			sb.append("  <meta charset=\"utf-8\">\n");
			sb.append("  <title>Список валют</title>\n");
			sb.append(" </head>\n");
			sb.append(" <body>\n");
			sb.append("  <table border=\"1\">\n");
			sb.append("   <caption>Список валют</caption>\n");
					
			sb.append("   <tr>\n");
			sb.append("    <th>Id</th>\n");
			sb.append("    <th>Код ISO</th>\n");
			sb.append("    <th>Цифровой код</th>\n");
			sb.append("    <th>Наименование</th>\n");
			sb.append("   </tr>\n");
			
			for (Currency cur : curList)
			{
				sb.append("   <tr>");
				sb.append("<td>").append(cur.getId()).append("</td>");
				sb.append("<td>").append(cur.getCode()).append("</td>");
				sb.append("<td>").append(cur.getCodeNum()).append("</td>");
				sb.append("<td>").append(cur.getName()).append("</td>");
				sb.append("</tr>\n");
			}
			
			sb.append("  </table>\n");
			sb.append(" </body>\n");
			sb.append("</html\n>");
		}
		
		response.getWriter().print(sb.toString());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

}
