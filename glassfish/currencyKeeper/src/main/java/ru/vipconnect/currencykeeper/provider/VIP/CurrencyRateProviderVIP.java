package ru.vipconnect.currencykeeper.provider.VIP;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.vipservice.sugarCRM.SugarClient;

import ru.vipconnect.currencykeeper.ejb.ConfigBean;
import ru.vipconnect.currencykeeper.entity.CurrencyRate;
import ru.vipconnect.currencykeeper.provider.CurrencyRateProvider;
import ru.vipconnect.currencykeeper.provider.VIP.IntExRates.IntExRate;

@Singleton
@Startup
@LocalBean
public class CurrencyRateProviderVIP extends CurrencyRateProvider
{
	private static final Logger log = LogManager.getLogger(CurrencyRateProviderVIP.class);

	@Override
	protected Integer getNewRates(List<String[]> currencyPairList)
	{
		log.info("Getting VIP rates...");
		int num = 0;
		
		
		try
		{
			String url = config.getProperty("provider." + getProviderCode().toLowerCase() + ".url");
			String username = config.getProperty("provider." + getProviderCode().toLowerCase() + ".username");
			String pwd = config.getProperty("provider." + getProviderCode().toLowerCase() + ".password");
			pwd = ConfigBean.decrypt(username + getProviderCode(), pwd);

			log.info("Params: " + url+ ", " + username + ", " +  pwd);
			SugarClient sc = new SugarClient(url, username, pwd, "CurrencyKeeper");
			System.out.println(sc.login());
			
			String xml = sc.exportAll("iexrt_internal_exchange_rate");
			
			log.info(xml);
			
			IntExRates iers = IntExRates.fromXml(xml);
			
			for (IntExRate ier : iers.getRowList())
			{
				for (String[] currencyPair : currencyPairList)
				{
					if (ier.getCurrencyCodeFrom().toUpperCase().equals(currencyPair[0]) && 
							ier.getCurrencyCodeTo().toUpperCase().equals(currencyPair[1]))
					{
						CurrencyRate cr = crBean.findByAK(currencyPair, ier.getRateDate(), getProviderCode());
						if (cr == null)
						{
							cr = new CurrencyRate();
							cr.setCurrencyFrom(cBean.findByCode(currencyPair[0]));
							cr.setCurrencyTo(cBean.findByCode(currencyPair[1]));
							cr.setNominal(ier.getNominal());
							cr.setValue(ier.getRateValue());
							cr.setDate(ier.getRateDate());
							cr.setProviderCode(getProviderCode());
							log.info("Inserting rate for " + currencyPair[0] + ", date: " + ier.getRateDate() + ", value:" + ier.getRateValue());
							crBean.insert(cr);
							num++;
						}
					}
				}				
			}
			
			sc.logout();
		}
		catch (Exception e)
		{
			log.error("Exception: ", e);		
		}

		log.info("Getting VIP rates...done!");
		log.info("Got " + num + " new rates");
		
		return num;		
	}
	
	@Override
	public String getProviderCode()
	{
		return "VIP";
	}

	@Override
	protected Logger getLogger()
	{
		return log;
	}
}
