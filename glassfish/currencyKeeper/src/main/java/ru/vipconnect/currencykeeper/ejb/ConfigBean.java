
package ru.vipconnect.currencykeeper.ejb;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ru.vipconnect.currencykeeper.entity.SysProperty;
import ru.vipconnect.currencykeeper.exception.CurrencyKeeperException;

/**
 * Чтение свойств на старте
 */
@Startup
@Singleton
@DependsOn("SysPropertyBLBean")
public class ConfigBean extends Properties {
	
	private static final long serialVersionUID = 1675175292718115910L;
	private static final Logger log = LogManager.getLogger(ConfigBean.class);
    Properties property = new Properties();    

    @EJB
    private SysPropertyBLBean spBean;
    
	public ConfigBean() {
	}

	@PostConstruct
	private void init()
	{
		InputStream is = null;
		String path = null;
		String propFileName = null;
		
		try
		{
			try
			{
				String appName = propFileName = (String) new InitialContext().lookup("java:app/AppName");

				if (appName != null)
				{
					propFileName = appName.toLowerCase() + ".properties";
				};
				log.info("Load " + propFileName);
				path = System.getProperty("com.sun.aas.instanceRoot") + "/config/" + propFileName;
				
				is = new FileInputStream(new File(path));
				
				Properties prop = new Properties();
				prop.load(is);
			
				this.putAll(prop);
				
				log.info("Load tbl_sys_property");
				for (SysProperty sp : spBean.listAllByConfig(appName.toLowerCase()))
				{
					if (this.getProperty(sp.getPk().getPropertyName()) != null)
					{
						throw new CurrencyKeeperException("Duplicate property: " + sp.getPk().getPropertyName());
					}
					else
					{	
						log.info(sp.getPk().getPropertyName() + " : " + sp.getPropertyValue());
						this.setProperty(sp.getPk().getPropertyName(), sp.getPropertyValue());
					}
				}
			}
			catch (NamingException | IOException | CurrencyKeeperException e)
			{
					log.error("Error loading "+ propFileName + ": " + path, e);
					throw new EJBException("Error loading " + propFileName + ": " + path, e);
			}
		}
		finally
		{
			if (is != null)
			try
			{
				is.close();
			}
			catch (IOException e){}
		}
	}	
	
	public void reload()
	{
		init();
	}
	
	public static String decrypt(String key, String source)
	{
		if (source != null && source.startsWith("#"))
		{
			try
			{
				IvParameterSpec iv = new IvParameterSpec("0123456789ABCDEF".getBytes());
				StringBuffer key128bit = new StringBuffer(key);
				for (int i = key.length(); i<16; i++) key128bit.append("0");
				key128bit.setLength(16);
				SecretKeySpec skeySpec = new SecretKeySpec(key128bit.toString().getBytes("UTF-8"), "AES");
	
				Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
				cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
	
				byte[] original = cipher.doFinal(new Base64().decode(source.substring(1)));
	
				return new String(original);
			}
			catch (Exception ex)
			{
				return source;
			}
		}
		else
		{
			return source;
		}
	}	
}
