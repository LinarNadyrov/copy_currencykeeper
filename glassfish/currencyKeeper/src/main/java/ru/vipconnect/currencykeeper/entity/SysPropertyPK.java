package ru.vipconnect.currencykeeper.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class SysPropertyPK implements Serializable
{
	private static final long serialVersionUID = 692009036361498465L;
	
	private String configName;
	private String propertyName;
	
	/** Наименование конфигурации */
	@Column(name="config_name")
	@NotNull	
	public String getConfigName()
	{
		return configName;
	}
	public void setConfigName(String configName)
	{
		this.configName = configName;
	}
	
	/** Наименование параметра */
	@Column(name="property_name")
	@NotNull	
	public String getPropertyName()
	{
		return propertyName;
	}
	public void setPropertyName(String propertyName)
	{
		this.propertyName = propertyName;
	}
}
