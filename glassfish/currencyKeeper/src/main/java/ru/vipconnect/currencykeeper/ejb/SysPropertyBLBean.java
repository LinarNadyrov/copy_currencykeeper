package ru.vipconnect.currencykeeper.ejb;

import java.util.List;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ru.vipconnect.currencykeeper.entity.SysProperty;

@Startup
@Singleton
public class SysPropertyBLBean
{
	@PersistenceContext(unitName = "currencyKeeper")
	protected EntityManager em;
	
	public List<SysProperty> listAllByConfig(String configName)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<SysProperty> cq = cb.createQuery(SysProperty.class);
		Root<SysProperty> c = cq.from(SysProperty.class);
		cq.select(c);
		cq.where(cb.equal(c.get("pk").get("configName"), configName));		
		
		return em.createQuery(cq).getResultList();		
	}	
}
