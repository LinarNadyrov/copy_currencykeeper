
package ru.vipconnect.currencykeeper.guvnor;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.drools.KnowledgeBase;
import org.drools.runtime.StatefulKnowledgeSession;

import ru.vipconnect.currencykeeper.ejb.ConfigBean;
import ru.vipconnect.currencykeeper.model.CurrencyPairSrcParams;

@Stateless
@LocalBean
public class DecisionTablesHelper
{

	private static Logger log = LogManager.getLogger(DecisionTablesHelper.class);

	@EJB
	private KnowledgePackagesSource knowledgePackagesSource;
	@EJB
	private ConfigBean config;

	public List<String[]> getCurrencyPairsForProvider(String providerCode)
			throws ApplyChangeSetException, RuleNotFoundException, InstantiationException, IllegalAccessException
	{
		CurrencyPairSrcParams p = new CurrencyPairSrcParams();
		
		p.setProvider(providerCode);
		
		KnowledgeBase kb = knowledgePackagesSource.getKnowledgeBase(config.getProperty("guvnor.currency_pair_package"));
		
		StatefulKnowledgeSession ksession = kb.newStatefulKnowledgeSession();
		
		TrackingAgendaEventListener trackingAgendaEventListener = new TrackingAgendaEventListener();
		
		log.debug(p);
		
		ksession.insert(p);
		ksession.addEventListener(trackingAgendaEventListener);
		ksession.fireAllRules();
		ksession.removeEventListener(trackingAgendaEventListener);
		ksession.dispose();
		
		return p.getCurrencyPairList();		
	}
}
