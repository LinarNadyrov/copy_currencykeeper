package ru.vipconnect.currencykeeper.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ru.vipconnect.currencykeeper.ejb.CurrencyRateBLBean;
import ru.vipconnect.currencykeeper.entity.CurrencyRate;

/**
 * Returns rate on date
 */
@WebServlet("/CurrencyRateOnDate")
public class CurrencyRateOnDate extends HttpServlet
{
	private static final long serialVersionUID = 2166105877318041773L;

	@EJB
	private CurrencyRateBLBean curRateBL;
	
	public CurrencyRateOnDate()
	{
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json");
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		StringBuffer sb = new StringBuffer();

		try
		{
			String providerCode = request.getParameter("provider_code");
			String currencyCodeFrom = request.getParameter("currency_code_from");
			String currencyCodeTo = request.getParameter("currency_code_to");
			Date date = sdf.parse(request.getParameter("date"));

			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();

			CurrencyRate cr = curRateBL.findRateOnDate(currencyCodeFrom, currencyCodeTo, date, providerCode);
			
			if (cr != null)
			{
				sb.append(gson.toJson(cr));
			}
			else
			{
				sb.append("{\n\tError: \"Курс не найден\"\n}");				
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			
			sb.append("{\n\tError: \"Ошибка обработки запроса\"\n}");
		}
		
		response.getWriter().append(sb.toString());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
