
package ru.vipconnect.currencykeeper.guvnor;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.drools.SystemEventListener;

@LocalBean
@Singleton
public class KAgentSystemEventListener implements SystemEventListener
{

	private static Logger log = LogManager.getLogger(KAgentSystemEventListener.class);

	@Override
	public void info(String message)
	{
	}

	@Override
	public void info(String message, Object object)
	{
	}

	@Override
	public void warning(String message)
	{
	}

	@Override
	public void warning(String message, Object object)
	{
	}

	@Override
	public void exception(String message, Throwable e)
	{
		log.fatal(e);
	}

	@Override
	public void exception(Throwable e)
	{
		log.fatal(e);
	}

	@Override
	public void debug(String message)
	{
	}

	@Override
	public void debug(String message, Object object)
	{
	}

}
