package ru.vipconnect.currencykeeper.provider;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Timeout;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import org.apache.logging.log4j.Logger;

import ru.vipconnect.currencykeeper.ejb.ConfigBean;
import ru.vipconnect.currencykeeper.ejb.CurrencyBLBean;
import ru.vipconnect.currencykeeper.ejb.CurrencyRateBLBean;
import ru.vipconnect.currencykeeper.guvnor.ApplyChangeSetException;
import ru.vipconnect.currencykeeper.guvnor.DecisionTablesHelper;
import ru.vipconnect.currencykeeper.guvnor.RuleNotFoundException;

public abstract class CurrencyRateProvider
{
	@EJB
	protected ConfigBean config;
	@EJB
	protected CurrencyBLBean cBean;
	@EJB
	protected CurrencyRateBLBean crBean;
	@EJB
	protected DecisionTablesHelper dth;
	
	@Resource
	private TimerService timerService;
	
	@PostConstruct
	public void init()
	{
		if (Boolean.valueOf(config.getProperty("provider." + getProviderCode().toLowerCase() + ".enable")))
		{
			try
			{
				updateRates();
			}
			catch (RuleNotFoundException | InstantiationException | IllegalAccessException | ApplyChangeSetException e)
			{
				getLogger().error("Exception: ", e);
			}
			
			ScheduleExpression schedule = new ScheduleExpression();
			schedule.minute(config.getProperty("provider." + getProviderCode().toLowerCase() + ".minute"));
			schedule.hour(config.getProperty("provider." + getProviderCode().toLowerCase() + ".hour"));
			TimerConfig timerConf = new TimerConfig();
			timerConf.setPersistent(false);
			if (schedule.getMinute() != null)
				timerService.createCalendarTimer(schedule, timerConf);
		}
	}

	@Timeout
	public void updateRates() throws RuleNotFoundException, InstantiationException, IllegalAccessException, ApplyChangeSetException
	{
		getNewRates(dth.getCurrencyPairsForProvider(getProviderCode()));
	}
	
	protected abstract String getProviderCode();

	protected abstract Integer getNewRates(List<String[]> currencyPairList);

	protected abstract Logger getLogger();
}
