package ru.vipconnect.currencykeeper.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ru.vipconnect.currencykeeper.ejb.CurrencyRateBLBean;
import ru.vipconnect.currencykeeper.entity.CurrencyRate;

/**
 * Returns list of currency rates
 */
@WebServlet("/CurrencyRateList")
public class CurrencyRateList extends HttpServlet
{
	private static final long serialVersionUID = 174181867202156533L;

	@EJB
	private CurrencyRateBLBean curRateBL;
	
	public CurrencyRateList()
	{
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setCharacterEncoding("utf-8");
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		
		String format = request.getParameter("format");
		String providerCode = request.getParameter("provider_code");
		String currencyCodeFrom = request.getParameter("currency_code_from");
		String currencyCodeTo = request.getParameter("currency_code_to");
		
		StringBuffer sb = new StringBuffer();
		
		try
		{
			Date dateFrom = null;
			Date dateTo = null;
			
			if (request.getParameter("date_from") != null)
				dateFrom = sdf.parse(request.getParameter("date_from"));
			if (request.getParameter("date_to") != null)
				dateTo = sdf.parse(request.getParameter("date_to"));
			
			List<CurrencyRate> crList = curRateBL.listOnRange(providerCode, dateFrom, dateTo, currencyCodeFrom, currencyCodeTo);
			
			if (format != null && "json".equals(format.toLowerCase()))
			{
				Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
				
				response.setContentType("application/json");
				
				sb.append("[\n");

				for (CurrencyRate cr : crList)
				{
					sb.append(gson.toJson(cr)).append(",\n");
				}
				
				sb.append("]\n");
			}
			else
			{
				response.setContentType("text/html");
				
				sb.append("<!DOCTYPE HTML>\n");
				sb.append("<html>\n");
				sb.append(" <head>\n");
				sb.append("  <meta charset=\"utf-8\">\n");
				sb.append("  <title>Курсы валют</title>\n");
				sb.append(" </head>\n");
				sb.append(" <body>\n");
				sb.append("  <table border=\"1\">\n");
				sb.append("   <caption>Курсы валют</caption>\n");
						
				sb.append("   <tr>\n");
				sb.append("    <th>Id</th>\n");
				sb.append("    <th>Дата</th>\n");
				sb.append("    <th>Валюта из</th>\n");
				sb.append("    <th>Валюта в</th>\n");
				sb.append("    <th>Номинал</th>\n");
				sb.append("    <th>Курс</th>\n");
				sb.append("    <th>Поставщик курса</th>\n");
				sb.append("   </tr>\n");
				
				for (CurrencyRate cr : crList)
				{
					sb.append("   <tr>");
					sb.append("<td>").append(cr.getId()).append("</td>");
					sb.append("<td>").append(sdf.format(cr.getDate())).append("</td>");
					sb.append("<td>").append(cr.getCurrencyFrom().getCode()).append("</td>");
					sb.append("<td>").append(cr.getCurrencyTo().getCode()).append("</td>");
					sb.append("<td>").append(cr.getNominal()).append("</td>");
					sb.append("<td>").append(cr.getValue()).append("</td>");
					sb.append("<td>").append(cr.getProviderCode()).append("</td>");
					sb.append("</tr>\n");
				}
				
				sb.append("  </table>\n");
				sb.append(" </body>\n");
				sb.append("</html\n>");				
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			
			if (format != null && "json".equals(format.toLowerCase()))
			{
				response.setContentType("application/json");
				sb.append("{\n\tError: \"Ошибка обработки запроса\"\n}");
			}
			else
			{
				response.setContentType("text/html");
	
				sb.append("<!DOCTYPE HTML>\n");
				sb.append("<html>\n");
				sb.append(" <head>\n");
				sb.append("  <meta charset=\"utf-8\">\n");
				sb.append("  <title>Error</title>\n");
				sb.append(" </head>\n");
				sb.append(" <body>\n");
				sb.append("  Ошибка обработки запроса");
				sb.append(" </body>\n");
				sb.append("</html\n>");
			}
		}
		
		response.getWriter().print(sb.toString());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}
}
