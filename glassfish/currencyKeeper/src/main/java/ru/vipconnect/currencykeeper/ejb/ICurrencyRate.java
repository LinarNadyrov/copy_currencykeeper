package ru.vipconnect.currencykeeper.ejb;

import java.util.Date;

import javax.ejb.Remote;

import ru.vipconnect.currencykeeper.entity.CurrencyRate;

@Remote
public interface ICurrencyRate
{
	public CurrencyRate findRateOnDate(String currencyCodeFrom, String currencyCodeTo, Date date, String providerCode);
}
