
package ru.vipconnect.currencykeeper.guvnor;


import static ru.vipconnect.currencykeeper.guvnor.KnowledgePackagesSource.removeFromKAgentCache;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.drools.event.knowledgeagent.AfterChangeSetAppliedEvent;
import org.drools.event.knowledgeagent.AfterChangeSetProcessedEvent;
import org.drools.event.knowledgeagent.AfterResourceProcessedEvent;
import org.drools.event.knowledgeagent.BeforeChangeSetAppliedEvent;
import org.drools.event.knowledgeagent.BeforeChangeSetProcessedEvent;
import org.drools.event.knowledgeagent.BeforeResourceProcessedEvent;
import org.drools.event.knowledgeagent.KnowledgeAgentEventListener;
import org.drools.event.knowledgeagent.KnowledgeBaseUpdatedEvent;
import org.drools.event.knowledgeagent.ResourceCompilationFailedEvent;
import org.drools.io.impl.UrlResource;



@LocalBean
@Singleton
public class KAgentEventListener implements KnowledgeAgentEventListener {
				
			
	private static  Logger log = LogManager.getLogger(KAgentEventListener.class);

		@Override
		public void afterChangeSetApplied(AfterChangeSetAppliedEvent event) {
			try {
				log.debug("AfterChangeSetAppliedEvent");
				if (event.getChangeSet().getResourcesModified().size() > 0) {
					log.info("RESOURCE MODIFIED: " + event.getChangeSet().getResourcesModified());
				}
				if (event.getChangeSet().getResourcesRemoved().size() > 0) {
					UrlResource res = (UrlResource) event.getChangeSet().getResourcesRemoved().iterator().next();
					String packageName = res.getURL().getPath().replaceAll("([a-zA-Z://.0-9]*(package/))|(/LATEST)$", "");
					removeFromKAgentCache(packageName);
					log.info("RESOURCE REMOVED: " + event.getChangeSet().getResourcesRemoved());
					//Email.send("Удален ресурс Drools", getEmailMessage(packageName), cfg);
				}
				if (event.getChangeSet().getResourcesAdded().size() > 0) {
					log.info("RESOURCE ADDED: " + event.getChangeSet().getResourcesAdded());
				}
			} catch (IOException ex) {
				log.fatal( ex);
			}
		}

		@Override
		public void beforeChangeSetApplied(BeforeChangeSetAppliedEvent event) {
		}

		@Override
		public void beforeChangeSetProcessed(BeforeChangeSetProcessedEvent event) {
		}

		@Override
		public void afterChangeSetProcessed(AfterChangeSetProcessedEvent event) {
		}

		@Override
		public void beforeResourceProcessed(BeforeResourceProcessedEvent event) {
		}

		@Override
		public void afterResourceProcessed(AfterResourceProcessedEvent event) {
		}

		@Override
		public void knowledgeBaseUpdated(KnowledgeBaseUpdatedEvent event) {
		}

		@Override
		public void resourceCompilationFailed(ResourceCompilationFailedEvent event) {
		}

		protected String getEmailMessage(String packageName) {
			InetAddress ip = null;
			try {
				ip = InetAddress.getLocalHost();
			} catch (UnknownHostException ex) {
				log.error( ex);
			}
			return "Сканером ресурсов удален пакет Drools: " + packageName + "<BR>"
				   + "Server IP address: " + ip.getHostAddress();
		}
}
