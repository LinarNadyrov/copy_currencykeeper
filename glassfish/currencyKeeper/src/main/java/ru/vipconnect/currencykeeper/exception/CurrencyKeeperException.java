package ru.vipconnect.currencykeeper.exception;

public class CurrencyKeeperException extends Exception
{
	private static final long serialVersionUID = -5726239469751420652L;
	
	public CurrencyKeeperException(String s)
	{
		super(s);
	}
}
